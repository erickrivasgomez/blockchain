<?php

include_once 'blockchain.php';

class apiblockchain
{

    private $error;


    public function registrar($datos_usuario)
    {
        $usuario = new Blockchain();
        $res = $usuario->registrarUsuario($datos_usuario);

        return $res;
    }

    public function autenticar($credenciales)
    {
        $usuario = new Blockchain();
        $respuesta = array();

        $res = $usuario->autenticarUsuario($credenciales);

        if ($res->rowCount()) {
            while ($row = $res->fetch(PDO::FETCH_ASSOC)) {

                $item = array(
                    "nombre" => $row["nombre"],
                );
                $respuesta = $item;
            }
            $this->printJSON($respuesta);
        } else {
            $this->error($res);
        }
    }

    public function obtenerNombre($nombre)
    {
        $usuario = new Blockchain();
        $respuesta = array();

        $res = $usuario->obtenerNombre($nombre);

        if ($res->rowCount()) {
            while ($row = $res->fetch(PDO::FETCH_ASSOC)) {
                $item = array(
                    "nombre" => $row['nombre'],
                );
                $respuesta = $item;
            }
            $this->printJSON($respuesta);
        } else {
            $this->error(null);
        }
    }

    public function obtenerSaldo($nombre)
    {
        $usuario = new Blockchain();
        $respuesta = array();

        $res = $usuario->obtenerSaldo($nombre);

        if ($res->rowCount()) {
            while ($row = $res->fetch(PDO::FETCH_ASSOC)) {

                $item = array(
                    "saldo" => $row['saldo'],
                );
                $respuesta = $item;
            }
            $this->printJSON($respuesta);
        } else {
            $this->error($res);
        }
    }

    public function registrarTransaccion($transaccion)
    {
        $usuario = new Blockchain();
        $res = $usuario->registrarTransaccion($transaccion);

        return $res;
    }

    public function obtenerTransacciones($nombre)
    {
        $transaccion = new Blockchain();
        $transacciones = array();
        $transacciones["transacciones"] = array();

        $res = $transaccion->obtenerTransacciones($nombre);

        $cont = 1;
        if ($res->rowCount()) {

            while ($row = $res->fetch(PDO::FETCH_ASSOC)) {

                $item = array(
                    "id" => $cont,
                    "fecha" => $row['fecha'],
                    "monto" => $row['monto'],
                    "certificado" => $row['certificado'],
                    "comprobado" => $row['comprobado'],
                );
                array_push($transacciones["transacciones"], $item);
                $cont = $cont + 1;
            }

            $this->printJSON($transacciones);
        } else {
            $this->error('No hay elementos');
        }
    }

    public function error($mensaje)
    {
        echo '<code>' . json_encode(array('mensaje' => $mensaje)) . '</code>';
    }

    public function exito($mensaje)
    {
        echo '<code>' . json_encode(array('mensaje' => $mensaje)) . '</code>';
    }

    public function printJSON($array)
    {
        echo json_encode($array);
    }

    public function getError()
    {
        return $this->error;
    }
}
