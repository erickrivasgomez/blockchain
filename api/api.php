<?php
include_once 'apiblockchain.php';

switch ($_REQUEST["accion"]) {
    case 'registrarUsuario':
        $apiblockchain = new apiblockchain();
        $usuario = array(
            "nombre" => $_POST["nombre"],
            "firma" => $_POST["firma"],
            "password" => $_POST["password"],
            "deposito" => $_POST["deposito"],
        );
        $apiblockchain->registrar($usuario);
        break;
    case 'autenticarUsuario':
        $apiblockchain = new apiblockchain();
        $usuario = array(
            "nombre" => $_POST["nombre"],
            "password" => $_POST["password"],
        );
        $apiblockchain->autenticar($usuario);
        break;
    case 'obtenerNombre':
        $apiblockchain = new apiblockchain();
        $apiblockchain->obtenerNombre($_POST["nombre"]);
        break;
    case 'obtenerSaldo':
        $apiblockchain = new apiblockchain();
        $apiblockchain->obtenerSaldo($_POST["nombre"]);
        break;
    case 'obtenerTransacciones':
        $apiblockchain = new apiblockchain();
        $apiblockchain->obtenerTransacciones($_POST["nombre"]);
        break;
    case 'registrarTransaccion':
        $apiblockchain = new apiblockchain();
        $transaccion = array(
            "nombre" => $_POST["nombre"],
            "monto" => $_POST["monto"],
        );
        $apiblockchain->registrarTransaccion($transaccion);
        break;
    default:
        # code...
        break;
}
