<?php

class DB
{
    private $host;
    private $db;
    private $user;
    private $password;

    public function __construct()
    {
        $this->host = ''; // host de la base de datos
        $this->db = ''; // nombre de la base de datos
        $this->user = ''; // usuario de la base de datos
        $this->password = ""; // password en la base de datos
    }

    public function connect()
    {

        try {

            $connection = "mysql:host=" . $this->host . ";dbname=" . $this->db;
            $options = [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_EMULATE_PREPARES => false,
            ];
            $pdo = new PDO($connection, $this->user, $this->password);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

            return $pdo;

        } catch (PDOException $e) {
            print_r('Error connection: ' . $e->getMessage());
        }
    }
}
