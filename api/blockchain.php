<?php

include_once 'db.php';

class blockchain extends DB
{

    public function registrarUsuario($usuario)
    {
        $query = $this->connect()->prepare('INSERT INTO usuarios (nombre, password, firma) VALUES (:nombre, md5(:password), :firma)');
        $query->execute([
            'nombre' => $usuario['nombre'],
            'password' => $usuario['password'],
            'firma' => $usuario['firma'],
        ]);

        $transaccion = array(
            'nombre' => $usuario['nombre'],
            'monto' => $usuario['deposito'],
        );

        $fecha = date('Y-m-d H:i:s');
        $query = $this->connect()->prepare(
            'INSERT INTO
                `transacciones`(`nombre_usuario`, `certificado`, `monto`, `fecha`, `comprobado`, `puntero`)
                select :nombre_usuario,:certificado,:monto,:fecha,false,0'
        );
        $query->execute([
            'nombre_usuario' => $transaccion['nombre'],
            'certificado' => md5($transaccion['nombre'].$transaccion['monto'].$fecha),
            'monto' => $transaccion['monto'],
            'fecha' => $fecha,
        ]);
        return $query;
    }

    public function autenticarUsuario($credenciales)
    {
        $query = $this->connect()->prepare('SELECT nombre FROM usuarios WHERE nombre = :nombre AND password = md5(:password)');
        if(!$query->execute([
            'nombre' => $credenciales['nombre'],
            'password' => $credenciales['password'],
        ])){
            print_r($query->errorInfo());
        }
        return $query;
    }

    public function obtenerNombre($nombre)
    {
        $query = $this->connect()->prepare('SELECT nombre AS nombre FROM usuarios WHERE nombre = :nombre');
        $query->execute([
            'nombre' => $nombre,
        ]);
        return $query;
    }

    public function obtenerSaldo($nombre)
    {
        $query = $this->connect()->prepare('SELECT SUM(monto) AS saldo FROM transacciones WHERE nombre_usuario = :nombre');
        $query->execute([
            'nombre' => $nombre,
        ]);
        return $query;
    }

    public function obtenerTransacciones($nombre)
    {
        $query = $this->connect()->prepare('SELECT * FROM transacciones WHERE nombre_usuario = :nombre');
        $query->execute([
            'nombre' => $nombre,
        ]);
        return $query;
    }

    public function registrarTransaccion($transaccion)
    {
        $fecha = date('Y-m-d H:i:s');
        $query = $this->connect()->prepare(
            'INSERT INTO
                `transacciones`(`nombre_usuario`, `certificado`, `monto`, `fecha`, `comprobado`, `puntero`)
                select :nombre_usuario,:certificado,:monto,:fecha,false,tr.id from transacciones tr where nombre_usuario = :nombre_usuario2 ORDER BY id desc limit 1'
        );
        $query->execute([
            'nombre_usuario' => $transaccion['nombre'],
            'certificado' => md5($transaccion['nombre'].$transaccion['monto'].$fecha),
            'monto' => $transaccion['monto'],
            'fecha' => $fecha,
            'nombre_usuario2' => $transaccion['nombre'],
        ]);
        return $query;
    }

    public function obtenerPeliculas()
    {
        $query = $this->connect()->query('SELECT * FROM pelicula');
        return $query;
    }

    /*

INSERT INTO `transacciones`(`id_usuario`, `certificado`, `monto`, `fecha`, `comprobado`, `puntero`)
select 1,'dfdsggh',-10.90,now(),0,tr.id from transacciones tr where id_usuario = 1 ORDER BY id desc limit 1
 */

}
