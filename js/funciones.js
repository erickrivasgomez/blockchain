function registrarUsuario() {

  var datos = {
    accion: "registrarUsuario",
    nombre: $("#inputnombre").val(),
    firma: $("#inputfirma").val(),
    password: $("#inputpassword").val(),
    deposito: $("#inputdeposito").val()
  };
  $.ajax({
    url: "./api/index.php",
    method: "POST",
    data: datos,
    success: function (result) {
      alert("Se ha registrado correctamente");

    }
  });
}

function autenticarUsuario() {

  var datos = {
    accion: "autenticarUsuario",
    nombre: $("#loginnombre").val(),
    password: $("#loginpassword").val()
  };
  $.ajax({
    url: "./api/index.php",
    method: "POST",
    data: datos,
    success: function (result) {
      try {
        var resultado = JSON.parse(result);
        console.log(resultado);
        if (resultado != null) {
          localStorage.setItem("blockchain_nombre", resultado.nombre);
          location.href = "home.html";
        }
      } catch (error) {
        alert("Datos incorrectos");
      }
    }
  });
}

function obtenerNombre() {

  var datos = {
    accion: "obtenerNombre",
    nombre: localStorage.getItem("blockchain_nombre")
  };
  $.ajax({
    url: "./api/index.php",
    method: "POST",
    data: datos,
    success: function (result) {
      var resultado = JSON.parse(result);
      $('#nombre_usuario').append(resultado.nombre);
    }
  });
}

function obtenerSaldo() {

  $('#saldo_actual').html("");
  var datos = {
    accion: "obtenerSaldo",
    nombre: localStorage.getItem("blockchain_nombre")
  };
  $.ajax({
    url: "./api/index.php",
    method: "POST",
    data: datos,
    success: function (result) {
      var resultado = JSON.parse(result);
      $('#saldo_actual').append(resultado.saldo);
    }
  });
}

function obtenerTransacciones() {

  $('#transacciones').html("");
  var datos = {
    accion: "obtenerTransacciones",
    nombre: localStorage.getItem("blockchain_nombre")
  };
  $.ajax({
    url: "./api/index.php",
    method: "POST",
    data: datos,
    success: function (result) {
      var resultado = JSON.parse(result);
      console.log(resultado);
      resultado.transacciones.forEach(transaccion => {
        var row = '<tr class="text-center">';
        row += '<td>' + transaccion.fecha + '</td>';
        row += '<td>' + transaccion.certificado + '</td>';
        row += '<td>' + transaccion.monto + '</td>';
        var estado = transaccion.comprobado ? "Comprobado" : "No comprobado";
        row += '<td>' + estado + '</td>';
        row += '</tr>';
        $('#transacciones').append(row);
      });
    }
  });
}

function registrarTransaccion() {

  var monto = parseFloat($('#monto_transaccion').val());
  var nombre = localStorage.getItem("blockchain_nombre");
  if (isNaN(monto)) {
    alert("Monto no válido");
  } else {
    var datos = {
      accion: "registrarTransaccion",
      nombre: nombre,
      monto: monto
    };
    $.ajax({
      url: "./api/index.php",
      method: "POST",
      data: datos,
      success: function (result) {
        $('#monto_transaccion').html("");
        alert("Se ha registrado correctamente");
        obtenerSaldo(nombre);
        obtenerTransacciones(nombre);
      }
    });
  }
}